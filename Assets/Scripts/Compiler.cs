﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text;

public class Compiler : MonoBehaviour
{
    private readonly string _allowedCahr = "1234567890()+-/*,.";
    private string _stringToEdit = "";
    private string _resultText = "";
    private string _errortext;
    bool _error;

    private static readonly Dictionary<string, byte> _opdict = new Dictionary<string, byte>
            {{"(", 0}, {"+", 1}, {"-", 2},{"/", 3}, {"*", 4}};

    private readonly HashSet<string> _operators = new HashSet<string> { "+", "-", "/", "*" };

    private string _input;

    void OnGUI()
    {
        GUI.Label(new Rect(10, 10, 100, 20), "Значение выражения");
        GUI.Label(new Rect(10, 35, 100, 20), "Результат рассчёта");
        GUI.Label(new Rect(10, 55, 350, 60), _errortext);
        _stringToEdit = GUI.TextField(new Rect(110, 10, 200, 20), _stringToEdit, 25);
        _resultText = GUI.TextField(new Rect(110, 35, 200, 20), _resultText, 25);

        if (GUI.Button(new Rect(315, 10, 100, 20), "Рассчитать"))
        {
            _errortext = "";
            bool hooks = false;
            int hooksOpenCount = 0;
            int hooksCloseCount = 0;
            char[] inputText = new char[0];
            List<string> hooksList = new List<string>();
            _error = false;

            if (!string.IsNullOrEmpty(_stringToEdit))
            {
                _stringToEdit = _stringToEdit.Replace(" ", "");
                _stringToEdit = _stringToEdit.Replace(".", ",");
                inputText = _stringToEdit.ToCharArray();                
            }
            else Error("Ошибка: Вы не ввели выражение ", -2);
            for (int i = 0; i < inputText.Length; i++)
            {
                bool find = false;
                if (!_error)
                {
                    foreach (char a in _allowedCahr)
                    {
                        if (inputText[i] == a)
                        {
                            find = true;
                            break;
                        }
                    }
                }   //Проверяем допустимость символа
                else find = true;
                if (!find) Error("Ошибка: Введён недопустимый символ ", i); //Если символ недопустим
                if (inputText[i] >= '0' && inputText[i] <= '9')
                {
                    if (i != inputText.Length - 1)
                        if (inputText[i + 1].ToString() == "(") Error("Ошибка: Неверно расставлены операторы. ", i);
                }   //Если символ число
                else if (_operators.Contains(inputText[i].ToString()))
                {
                    if (i != inputText.Length - 1)
                        if (_operators.Contains(inputText[i + 1].ToString())) Error("Ошибка: Два оператора подряд. ", i);
                    if (i + 1 == inputText.Length || i == 0) Error("Ошибка: Неверно расставлены операторы. ", i);
                }   //Если символ оператор
                else if (inputText[i].ToString() == ")")
                {
                    hooksOpenCount++;
                    hooksList.Add(inputText[i].ToString());
                    hooks = true;
                    if (i != inputText.Length - 1)
                    {
                        if (_operators.Contains(inputText[i-1].ToString()))
                            Error("Ошибка: Неверно расставлены операторы. ", i);
                        if (inputText[i+1] >= '0' && inputText[i] <= '9')
                            Error("Ошибка: Неверно расставлены операторы. ", i);
                    }                    
                }   //Если символ закрывающая скобка
                else if (inputText[i].ToString() == "(")
                {
                    hooksCloseCount++;
                    hooksList.Add(inputText[i].ToString());
                    hooks = true;
                    if (i != inputText.Length - 1)
                    {
                        if (_operators.Contains(inputText[i + 1].ToString()))
                            Error("Ошибка: Неверно расставлены операторы. ", i);
                        if (inputText[i + 1].ToString() == ")") Error("Ошибка: Неверно расставлены операторы. ", i);
                    }
                }   //Если символ отрывающая скобка                
            }
            if (hooksCloseCount != hooksOpenCount) Error("Ошибка: Проверьте количество открывающих и закрывающих скобок. ", -2);
            if (hooks)
            {
                if (hooksList[0] == ")") Error("Ошибка: Первая скобка, является закрывающей. ", -2);
            }
            if (!_error)
            {
                string resultString = string.Concat<char>(inputText);
                _resultText = Calculate(resultString).ToString();
                _errortext = "Всё норм";
                _error = true;
            }
        }
    }

    private void Error(string text, int id)
    {
        if (id >= 0) text += "Позиция символа " + (id + 1);
        _error = true;
        _errortext = text;
        _resultText = "";
    }
    
    private double CalculateSimple(double a, double b, char op)
    //Выполняем нужное вычисление в зависимости от операции
    {
        switch (op)
        {
            case '+':
                return a + b;
            case '-':
                return a - b;
            case '*':
                return a * b;
            case '/':
                return a / b;
        }
        return 0;
    }

    private double StackCalc(double a, double b, char op)
    //Так как из стека достаем в обратном порядке, требуется инвертировать левый и правый операнды
    {
        return CalculateSimple(b, a, op);
    }
    
    public string ToRPN() //Для более удобного анализа переводим в ОПН (Обратная польская нотация)
    {
        var result = new StringBuilder(_input.Length);
        bool negative = false;
        var stack = new Stack<char>();

        for (int i = 0; i < _input.Length; i++)
        {
            if (_input[i] == ' ') continue;
            if (_input[i] == '(')
                stack.Push(_input[i]);
            else if (_input[i] == ')')
            //Если встречаем закрывающую скобку, то вытряхиваем в строку все, до появления открывабщей скобки
            {
                while (stack.Count > 0)
                {
                    char a = stack.Pop();
                    if (a == '(') break;
                    result.Append(" " + a);
                }
            }
            else if (_operators.Contains(_input[i].ToString()))
            //Иначе если это оператор, вытряхиваем все операторы с большим приоритетом в строку, после этого засовываемся сами в стек
            {
                string op = _input[i].ToString();
                if (_input[i] == '-')
                {
                    negative = true;
                    if (_input[i-1].ToString() == "(")
                        result.Append("0 "); //Используем математический подход a - b = a + (0 - b)
                    op = "+";
                }
                else
                    negative = false;
                result.Append(" ");
                while (stack.Count > 0 && _opdict[op] < _opdict[stack.Peek().ToString()])
                {
                    result.Append(stack.Pop() + " ");
                }
                stack.Push(op[0]);
            }
            else
            {
                if (negative)
                {
                    result.Append('-');
                    negative = false;
                }
                result.Append(_input[i]);
            } //Если это не скобки и не операторы, то это цифра, добавляем к выходной строке, с учетом знака
            print(result);
        }
        while (stack.Count > 0) //Вытряхиваем в строку оставшиеся символы
            result.Append(" " + stack.Pop());
        return result.ToString();
    }

    public double Calculate(string input)
    //Вычисляем выражение. Если формат ввода неправильный, возвращаем null
    {
        if (string.IsNullOrEmpty(input))
            return double.NaN;
        _input = input;
        string[] strings = ToRPN().Split(new[] { ' ' }, System.StringSplitOptions.RemoveEmptyEntries);
        var stack = new Stack<double>(input.Length / 2);
        foreach (string s in strings)
        {
            if (_opdict.ContainsKey(s)) //Если оператор, то выполняем его над последними двумя элементами стека.
            {
                if (stack.Count < 2)
                    if (s == "+" || s == "-")
                    {
                        stack.Push(StackCalc(stack.Pop(), 0, s[0]));
                        continue;
                    }
                    else return double.NaN;
                stack.Push(StackCalc(stack.Pop(), stack.Pop(), s[0]));
            }
            else //Иначе это должно быть число, если вдруг что-то другое, то возвращаем null
            {
                double a;
                if (!double.TryParse(s, out a)) return double.NaN;
                stack.Push(a);
            }
        }
        return stack.Peek();
        //В стеке остается единственный элемент, значение выражения, который возвращаем
    }
}
